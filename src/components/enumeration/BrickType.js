const BrickType = {
  PLAIN: 'PLAIN',
  PLURAL: 'PLURAL',
  MARKDOWN: 'MARKDOWN',
};

export default BrickType;
