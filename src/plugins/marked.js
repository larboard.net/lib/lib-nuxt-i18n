import realMarked from 'marked';

function marked(text) {
  const sanitized = text.replace(/<[^>]*>?/gm, '');
  return realMarked(sanitized);
}

export default function(ctx, inject) {
  inject('marked', marked);
}
